int spindle_pin = 0; // output for esc
int potentiometer_pin = 1; // speed control potentiometer central pin

int enable_pin = 3; // spindle enable pin
int debug_pin = 4; // debug enable pin

int initial_count = 0; // initial time counter
int initial_count_limit = 500; // multiples of 20 ms
int initial_count_done = 0; // flag for the inicial "off" time

float time_high; // pulse "on" time in seconds
float time_low; // pulso "off" time in seconds


void setup(){

	cli();		// disable interrupts

	TCCR0A = 0;		// set entire register to zero
	TCCR0B = 0;		// set entire register to zero
	TCNT0 = 0;		//set counter to zero

  	TCCR0A |= (1 << WGM01); 		// set CTC mode
  	TCCR0B |= (1 << CS02) | (1 << CS00);	// set 1024 prescaler

  	OCR0B = 8; 	// compare register initial value to avoid bugs during inicialization
  	OCR0A = 157;	// compare register inicial value to avoid bugs durin inicialization

  	TIMSK |= (1 << OCIE0A) | (1 << OCIE0B); // enable interruptions on compare match

  	pinMode(spindle_pin, OUTPUT); 			// set spindle pin as output
        pinMode(enable_pin, INPUT);
        pinMode(debug_pin, INPUT_PULLUP);               // set debug pin as input and enables internal pullup resitor


  	sei(); 		// enable interrupts
}


void loop(){
  
        if(digitalRead(debug_pin)==0){initial_count_done=1;} // verify debug mode
        
        if(initial_count >= initial_count_limit){initial_count_done = 1;} // checks if initial timer is done       
                                      
	time_high = 0.001*(1 + analogRead(potentiometer_pin)/1023.); // calculates pulse "on" time
	         
	cli(); // disable interrups - OCRnx changes makes everything a mess
        
        if(digitalRead(enable_pin)==1){ // checks if the enable pin are high
          
          if(initial_count_done==1){ // checks if inicial count is done
            
	    OCR0B = int((time_high*7812.5));  // update register value based on the time calculated from potentiometer
            OCR0A = 156;
          }
          else{ //if inicial count isn't done, sets zero speed
           OCR0B = 8; //int((0.001*7812.5)+0.5);
	   OCR0A = 156; //int((0.019*7812.5)+0.5); 
          }
            
        }
          
        else{
          
           OCR0B = 8; //int((0.001*7812.5)+0.5);
	   OCR0A = 156; //int((0.019*7812.5)+0.5);          
        }

	sei(); // enable interrupts now that OCRnx has been updated                 

}

ISR(TIMER0_COMPA_vect){
        digitalWrite(spindle_pin, HIGH); //write spindle output
        initial_count += 1; //initial "off" timer
}

ISR(TIMER0_COMPB_vect){
	digitalWrite(spindle_pin, LOW);
}

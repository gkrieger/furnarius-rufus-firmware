Furnarius Rufus PCI Milling Machine Firmware 
===================================

This repository contains the documentation for the Furnarius Rufus PCB Milling Machine Firmware(Fresadora PCB João-de-Barro in portuguese). It's a work in progress, based on the GRBL firmware for CNC machines using the Arduino UNO.

GRBL Firmware can be found at <https://github.com/grbl/grbl>


There is also a version for RAMPS, using the Arduino MEGA and the Marlin Firmware for 3D printers.

The Marlin Firmware can be found at <https://github.com/ErikZalm/Marlin>.


About the Authors
-----------------

This project started by [Centro de Tecnologia Acadêmica](http://cta.if.ufrgs.br) of Instituto de Física / Universidade Federal do Rio Grande do Sul (IF/UFRGS).

Gabriel Krieger Nardon - Main Firmware Developer

Germano Postal - Main Hardware Developter

Rafael Peretti Pezzi - Mentor

For more information, please visit the [Project website](http://cta.if.ufrgs.br/projects/fresadora-pci-joao-de-barro/wiki).

Licensing Information
---------------------

Furnarius Rufus PCB MIlling Machine Firmware
Copyright (C) 2014 by the Authors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
